#include "list.h"
#include "elmlist.h"
#include "outils.h"


#pragma warning(disable: 4996)


void listehomoreelle() {
    void (*ptrPrint) (double*);
    void (*ptrRm) (double*);
    bool (*ptrCmp) (double*, double*);
    struct list* L = consVide();
    double* v;
    double u;

    ptrPrint = &printDouble; // pointeur sur l�afficha d�un r�el
    ptrRm = &rmDouble; // pointeur sur la lib�ration d�un r�el
    ptrCmp = &reelcmp;

    do {
        printf("Entrez un r�el (0 pour s�arr�ter): ");
        scanf("%lf", &u);
        if (u == 0) break;
        v = (double*)calloc(1, sizeof(double));
        *v = u;
        //cons(L, v);

        insert_ordered(L, v, ptrCmp);
    } while (true);
    viewList(L, ptrPrint);
    rmList(L, ptrRm, true);
}

void listehomoentiere() {
    void (*ptrPrint) (int*);
    void (*ptrRm) (int*);
    struct list* L = consVide();
    int* v;
    int u;
    bool(*ptrCmp)(int*, int*);
    ptrPrint = &printInteger;
    ptrRm = &rmInteger;
    ptrCmp = &intcmp;
    do {
        printf("Entrez un entier (0 pour s�arr�ter): ");
        scanf("%d", &u);
        if (u == 0) break;
        v = (int*)calloc(1, sizeof(int));
        *v = u;
        //cons(L, v);
        insert_ordered ( L, v, ptrCmp );
    } while (true);
    viewList(L, ptrPrint);
    rmList(L, ptrRm, true);
}



int main() {

    listehomoentiere();
    listehomoreelle();

    getchar();

    return 0;
}