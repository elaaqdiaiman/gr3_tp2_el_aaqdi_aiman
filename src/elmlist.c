#include "elmlist.h"
#include "list.h"


/** R�cup�rer la *donn�e* d�un �l�ment de liste */
void* getData(struct elmlist* E) {
	return E->data;
}

/** R�cup�rer le *successeur* d�un �l�ment de liste */
struct elmlist* getSuc(struct elmlist* E) {
	return E->suc;
}


/** Modifier la *donn�e* d�un �l�ment de liste */
void setData(struct elmlist* E, void* d) {
	E->data = d;
}

/** Modifier le *successeur* d�un �l�ment de liste */
void setSuc(struct elmlist* E, struct elmlist* S) {
	E->suc = S;
}