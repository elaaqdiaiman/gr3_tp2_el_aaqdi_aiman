#include "list.h"
#include "elmlist.h"



/** V�rifie si la liste est vide ou pas */
bool estVide(struct list* L) {
	return L == consVide();
}

/** Construit une liste vide */
struct list* consVide() {


	struct list* new_list = (struct list*)calloc(1, sizeof(struct list));

	return new_list;

}

/** Ajoute en t�te de la liste un nouvel �l�ment dont la donn�e est �d� */
void cons(struct list* L, void* d) {
	
	struct elmlist* new_node = (struct elmlist*)malloc(sizeof(struct elmlist));


	if (estVide(L)) {

		setData(new_node, d);
		setSuc(new_node, NULL);

		L->head = new_node;
		L->tail = new_node;

		L->numelm = 1;


	}
	else {
		setData(new_node, d);
		setSuc(new_node, L->head);


		L->head = new_node;
		L->numelm++;
	}
}

/** Ajoute en queue de la liste un nouvel �l�ment dont la donn�e est �d� */
void queue(struct list* L, void* d) {
	
	struct elmlist* new_node = (struct elmlist*)malloc(sizeof(struct elmlist));
	
	setData(new_node, d);
	setSuc(new_node, NULL);

	if (estVide(L))
		L->numelm = 1;
	
	else {

		L->tail = new_node;
		L->numelm++;
	}
}


/** Consulte la t�te de la liste */
struct elmlist* getHead(struct list* L) {
	assert(L != NULL);
	return L->head;
}



/** Consulte la queue de la liste */
struct elmlist* getTail(struct list* L) {
	assert(L != NULL);
	return L->tail;
}


/** Consulte le nombre d��l�ments rang�s dans la liste */
int getNumelm(struct list* L) {
	assert(L != NULL);
	return L->numelm;
}


void viewList(struct list* L, void (*ptrf) ()) {
	for (struct elmlist* iterator = L->head; iterator != NULL; iterator = iterator->suc) {
		(*ptrf) (iterator->data);
	}
}

void rmList(struct list* L, void (*ptrF)(), bool withData) {

	if (withData == true) {

		struct elmlist* E;

		for (int i = 0; i < getNumelm(L); ++i) {
			E = L->head->suc;
			(*ptrF)(L->head->data);
			free(L->head);

			L->head = E;
		}
	}

	free(L);
}


	void insert_ordered(struct list* L, void* d, bool (*ptrf) ()) {

		if (estVide(L)) {
			cons(L, d);
		}

		else if ((*ptrf)(d, L->head->data)) {
			cons(L, d);
		}

		else {
			struct elmlist* p = L->head;

			while (p->suc != NULL && (*ptrf)(p->suc->data, d)) {
				p = p->suc;
			}
			insert_after(L, d, p);
		}
	}


void insert_after(struct list* ptrL, const int v, struct elmlist* P) {
	if (estVide(ptrL) || !P) {
		cons(ptrL, v);
	}
	else {
		struct elmlist* new_node = (struct elmlist*)calloc(1, sizeof(struct elmlist));

		setData(new_node, v);
		setSuc(new_node, P->suc);
		setSuc(new_node->suc, P->suc);
		setSuc(P, new_node);

		ptrL->numelm++;
	}
}