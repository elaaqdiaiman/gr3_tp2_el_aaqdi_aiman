/**************
TYPES ABSTRAITS
**************/
/** Le type d�un �l�ment de liste :
- �x� un *pointeur* sur une donn�e,
- �suc� un pointeur sur son successeur (ou NULL s�il n�y en a pas)
*/

#ifndef __ELMLIST_H__
#define __ELMLIST_H__

#include <stdio.h>
#include <stdlib.h>

struct elmlist {
	void* data;
	struct elmlist* suc;
};


/***********************************
D�CLARATIONS DES FONCTIONS PUBLIQUES
***********************************/
/** R�cup�rer la *donn�e* d�un �l�ment de liste */
void* getData(struct elmlist* E);
/** R�cup�rer le *successeur* d�un �l�ment de liste */
struct elmlist* getSuc(struct elmlist* E);
/** Modifier la *donn�e* d�un �l�ment de liste */
void setData(struct elmlist* E, void* d);
/** Modifier le *successeur* d�un �l�ment de liste */
void setSuc(struct elmlist* E, struct elmlist* S);

#endif // !__ELMLIST_H