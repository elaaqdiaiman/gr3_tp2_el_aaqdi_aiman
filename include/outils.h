#ifndef _OUTILS_
#define _OUTILS_

#include <stdbool.h>

/** Fonctions :
- D'affichage d'une valeur enti�re,
- De suppression d'une m�moire de type entier,
- De comparaison de 2 entiers.
 */
void printInteger(int* i);
void rmInteger(int* i);
bool intcmp(int* i, int* j);

/** Fonctions :
- D'affichage d'une valeur r�elle,
- De suppression d'une m�moire de type r�el,
- De comparaison de 2 r�els.
 */
void printDouble(double* d);
void rmDouble(double* d);
bool reelcmp(double* u, double* v);

#endif // _OUTILS_
