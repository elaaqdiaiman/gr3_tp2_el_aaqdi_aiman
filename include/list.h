#ifndef _INTLIST_
#define _INTLIST_

#include <stdlib.h> // librairie standard
#include <stdio.h> // librairie input/output
#include <stdbool.h> // librairie du type bool�en
#include <assert.h> // librairie d'assertions

/**************
TYPES ABSTRAITS
**************/
/** Le type d�un �l�ment de liste :
- �head� un *pointeur* sur le premier �l�ment de la liste
- �tail� un *pointeur* sur le dernier �l�ment de la liste
- �numelm� le nombre d��l�ments rang�s dans la liste
*/
struct list {
	struct elmlist* head;
	struct elmlist* tail;
	int numelm;
};
/***********************************
D�CLARATIONS DES FONCTIONS PUBLIQUES
***********************************/
/** V�rifie si la liste est vide ou pas */
bool estVide(struct list* L);
/** Construit une liste vide */
struct list* consVide();
/** Ajoute en t�te de la liste un nouvel �l�ment dont la donn�e est �d� */
void cons(struct list* L, void* d);
/** Ajoute en queue de la liste un nouvel �l�ment dont la donn�e est �d� */
void queue(struct list* L, void* d);
/** Consulte la t�te de la liste */
struct elmlist* getHead(struct list* L);
/** Consulte la queue de la liste */
struct elmlist* getTail(struct list* L);
/** Consulte le nombre d��l�ments rang�s dans la liste */
int getNumelm(struct list* L);
/**  ViewList*/
void viewList(struct list* L, void (*ptrf) ());


/** RmList*/
/** Supprimer une liste signifie :
- Soit que l�on veut supprimer **uniquement** la structure de liste ;
- Soit que l�on veut supprimer la structure de liste et ses donn�es.
Les listes �tant **g�n�riques** et **homog�nes**,
on utilise alors le _pointeur de fonction_ �ptrF� pour indiquer � �rmList�
la fonction de suppression qui correspond aux donn�es stock�es dans la liste :
- �rmInteger�
- �rmDouble�
de la biblioth�que outils.
On va utiliser le bool�en �withData� qui prend la valeur :
- �false� si l�on doit supprimer **uniquement** la structure de liste ou
- �true� si l�on doit �galement supprimer les donn�es.
*/
void rmList(struct list* L, void (*ptrF) (), bool withData);
/** InsertOrdered*/
void insert_ordered(struct list* L, void* d, void (*ptrf) ());
/** InsertAfter*/
void insert_after(struct list* ptrL, const int v, struct elmlist* P);

#endif // _INTLIST_